# Build application

```sh
docker build --tag=application .
```

# Run application

```sh
docker run -it application
```
