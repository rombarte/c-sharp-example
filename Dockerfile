FROM mcr.microsoft.com/dotnet/sdk:5.0

WORKDIR /src/
COPY *.csproj ./
COPY . ./src
RUN dotnet build -c Debug

ENTRYPOINT ["dotnet", "run", "-c", "Debug", "--no-build"]
